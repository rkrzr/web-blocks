#!/usr/bin/env python
"""
Maps the output from the boilerpipe segmentation algorithm back to the expected
format (i.e. HTML with classes indicating what's a block).
"""

#from html5lib import parse, treewalkers, serializer
from lxml.html import tostring, html5parser, fromstring
from lxml import etree
import lxml
import html5lib
import difflib
import codecs

#ContentOnlyElement = etree.__ContentOnlyElement

def parse_line(line):
	"""Parse one line of the boilerpipe results."""
	l = line.split(', [')
	try:
		content = l[0]
		classification = l[1].split('\t')[1].split(',')[0]
	except:
		# What is the meaning of the lines without classification?
		# Are they blocks, or are they nothing?
		# I will assume nothing for now, since otherwise they should have a classification
		# print 'Problematic line: ', line
		return None
	
	return (content, classification)

def parser(text):
	"""
	Boilerpipe puts the text of every block it found on one line, and it
	also indicates whether the block is 'boilerplate' or 'CONTENT'.
	We parse each line and return a list of pairs with each containing
	the text of a block and its classification.
	"""
	# the first and last two lines are boilerplate
	lines = text.split('\n')[1:-2]
	parsed = []
	
	for line in lines:
		p = parse_line(line)
		if p:
			parsed.append(p)

	return parsed

def treewalker(tree, texts):
	"""
	We walk the tree in document order once and inject block tags when we
	find a node that matches the text we are looking for.
	
	The modified tree is returned.
	"""
	if not texts:
		return tree
	
	# Pop the first text-block from the stack
	text, classification = texts.pop(0)
	
	# TODO: Check element.tail as well?
	# print 'texts: ', texts
	
	# Walk the tree in document order
#	for element in tree.iter():
#		node_text = tostring(element, encoding='unicode', method='text').strip()
#		
#		if node_text:			
#			# Check if the text of the whole node with children matches
#			if difflib.SequenceMatcher(None, text, node_text).ratio() > 0.8:
#				print text, 'MATCH'
#				add_block(element, classification)
#				if not texts: break # stack is empty
#				text, classification = texts.pop(0)
#			# Check if just the element text matches
#			elif element.text and difflib.SequenceMatcher(None, text, element.text.strip()).ratio() > 0.8:
#				print text, 'ELEMENT MATCH'
#				add_block(element, classification, wrap=True)
#				if not texts: break # stack is empty
#				text, classification = texts.pop(0)
#			else:
#				print 'fail: ', text
				
	# Brute-force mapping
	for text, classification in texts:
		for element in tree.iter():
			# FIXME: Why doesn't this work?
#			if isinstance(element, ContentOnlyElement):
#				continue
			
			node_text = tostring(element, encoding='unicode', method='text').strip()
			
			if node_text:
				# Check if the text of the whole node with children matches
				if difflib.SequenceMatcher(None, text, node_text).ratio() > 0.7:
					add_block(element, classification)
					break
				# Check if just the element text matches
				elif element.text and difflib.SequenceMatcher(None, text, element.text.strip()).ratio() > 0.7:
					add_block(element, classification, wrap=True)
					break

	return tree

def add_block(element, classification, wrap=False):
	"""
	Modifies the given tree element in-place by adding the block identifier
	and the type of the block.
	"""
	# Create a new element wrapping the text
	if wrap:
		text = element.text
		# Remove the text from the parent
		element.text = ''
		# Create new div wrapping the text
		new_element = etree.Element("div")
		try:
			element.insert(0, new_element)
		except Exception as e:
			print 'Failed to insert: ', element
		element = new_element
		element.text = text
		
	# Add block identifier
	try:
		element.set('data-block', '1')
		element.set('data-block-type', classification)
	except Exception as e:
		print 'Cannot add block-attributes: ', e

def mapper(html, parsed):
	# Parse html into an lxml.etree (lxml's enhanced version of an ElementTree)
	clean_html = strip_control_characters(html)
	parser = html5lib.HTMLParser(tree=html5lib.getTreeBuilder("lxml"), namespaceHTMLElements=False)
	tree = html5parser.fromstring(clean_html, parser=parser)
	
	# Append style to view blocks
	elem = etree.Element('style', type="text/css")
	elem.text = '[data-block] {outline: 1px solid blue !important; box-shadow: inset 0 0 0 10px rgba(220,221,222,1), 0 0 0 1px blue !important;}'
	tree.xpath('//head')[0].append(elem)
		
	# Inject the block tags into the tree
	new_tree = treewalker(tree, parsed)
			
	# Serialize the tree containing the blocks to unicode
	return tostring(new_tree, pretty_print=True, encoding='unicode')

def strip_control_characters(input):
	
	if input:
	
		import re
		
		# unicode invalid characters
		RE_XML_ILLEGAL = u'([\u0000-\u0008\u000b-\u000c\u000e-\u001f\ufffe-\uffff])' + \
						 u'|' + \
						 u'([%s-%s][^%s-%s])|([^%s-%s][%s-%s])|([%s-%s]$)|(^[%s-%s])' % \
						  (unichr(0xd800),unichr(0xdbff),unichr(0xdc00),unichr(0xdfff),
						   unichr(0xd800),unichr(0xdbff),unichr(0xdc00),unichr(0xdfff),
						   unichr(0xd800),unichr(0xdbff),unichr(0xdc00),unichr(0xdfff),
						   )
		input = re.sub(RE_XML_ILLEGAL, "", input)

		# ascii control characters  
#		input = re.sub(r"[\x01-\x1F\x7F]", "", input)

	return input

def main():	
	with codecs.open('/opt/dataset2/www.selectorgadget.com/www.selectorgadget.com/index.html', 'rU', 'utf-8') as f:
		html = f.read()
	with codecs.open('../test/result.txt', 'rU', 'utf-8') as f: # TODO: integrate directly with the algorithm
		content = f.read()
	
	parsed = parser(content)
	result = mapper(html, parsed)
	print result

if __name__ == '__main__':
	main()