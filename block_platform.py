#!/usr/bin/env python

"""
1. Run a local web server to serve the html documents: twistd -y -n web --path .
   (this is necessary, since some algorithms only take a url as input)


Input: (Algorithm, Dataset)
For each Page in Dataset do
	Blocks <- Algorithm(Page)
	BlockPage <- Mapper(Blocks)
	Result <- Compare(Page, BlockPage)
Output: Result
"""

import codecs
import os
import csv
import difflib
import sys

from chardet import detect
from html5lib import parse
from subprocess import Popen, PIPE

from algorithms.terrain import web_terrain
from algorithms import boilerpip, semantic_partitioner, vips
from mapping.boilerpip import parser, mapper
from lxml.html import tostring
from lxml import etree


# Serves the dataset dir (necessary since pywebkitgtk doesn't work with file:/// uris)
fileserver = 'http://localhost:8080'
filedir = os.path.dirname(__file__)

def run_algorithm(cmd=None, url=None):
	""" Run the given algorithm in a subprocess and get back the html. """

	cmd = cmd + ' ' + url
	p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
	stdout, stderr = p.communicate()
	
	return stdout


def strip_control_characters(input):
	
	if input:
	
		import re
		
		# unicode invalid characters
		RE_XML_ILLEGAL = u'([\u0000-\u0008\u000b-\u000c\u000e-\u001f\ufffe-\uffff])' + \
						 u'|' + \
						 u'([%s-%s][^%s-%s])|([^%s-%s][%s-%s])|([%s-%s]$)|(^[%s-%s])' % \
						  (unichr(0xd800),unichr(0xdbff),unichr(0xdc00),unichr(0xdfff),
						   unichr(0xd800),unichr(0xdbff),unichr(0xdc00),unichr(0xdfff),
						   unichr(0xd800),unichr(0xdbff),unichr(0xdc00),unichr(0xdfff),
						   )
		input = re.sub(RE_XML_ILLEGAL, "", input)

		# ascii control characters  
#		input = re.sub(r"[\x01-\x1F\x7F]", "", input)

	return input

def get_blocks(html):
	""" 
	Take the html, parse it, run the xpath to get the blocks on it and return
	them as a set.
	"""
	node_text = lambda x: tostring(x, encoding='unicode', method='text').replace('\r', '').replace('\n', '').replace(' ', '').strip()
	
	clean_html = strip_control_characters(html)
	tree = parse(clean_html, treebuilder='lxml', namespaceHTMLElements=False)
	elems = tree.xpath('//*[@data-block]')
	result = [node_text(elem) for elem in elems]  # We only take the element texts to compare
	
#	return set(elems)
	return set(result)

def evaluate(result, ground_truth, path, fuzzy=False):
	""" We compare the algorithmic result to the ground truth. """
	if fuzzy:
		good_hits = fuzzy_match(result, ground_truth) # fuzzy match metric
	else:
		good_hits = len(ground_truth.intersection(result)) # exact match metric
	
	relevant_results = len(ground_truth)
	retrieved_results = len(result)
	
	# Precision: How many blocks are relevant out of our prediction
	precision = good_hits / float(retrieved_results) if retrieved_results else 0
	
	# Recall: How many actual blocks did we catch
	recall = good_hits / float(relevant_results) if relevant_results else 0
	
	# F-score: The harmonic mean of precision and recall (ideally 1)
	fscore = (2 * precision * recall) / (precision + recall) if precision + recall else 0
	
	pinwords = str(good_hits) + ' relevant results out of ' + str(retrieved_results) + \
	           ' retrieved results.'
	rinwords = str(good_hits) + ' out of ' + str(relevant_results) + ' of all relevant results found.'
	finwords = 'The F-score, taking both precision and recall into account, is: ' + str(fscore)
	
	print '\n\n--------------RESULTS for ' + path + '--------------\n\n'
	
	print pinwords
	print rinwords
	print finwords + '\n'
	
	print 'Precision: ', precision 
	print 'Recall: ', recall
	print 'F-score: ', fscore, '\n\n'
	
	return [precision, recall, fscore, retrieved_results, good_hits, relevant_results]

def fuzzy_match(results, ground_truth):
	"""
	Takes two sets of strings as input. The strings are the serialized texts
	from the block nodes.
	
	We want to be able to detect close (if not exact) matches, e.g. when a
	block is wrapped in an additional div then this should still be a match.
	
	Criteria: e.g. if a block has no siblings and covers (almost) the same
	area as its parent. More generally: if it covers the same area as the
	parent.
	"""
	
	good_hits = 0
	hit = False
	for actual_block in ground_truth:
		for found_block in results:
			# TODO: Does SequenceMatcher do a char based diff??
			if difflib.SequenceMatcher(None, found_block, actual_block).ratio() > 0.8:
				good_hits += 1
				break
			
	return good_hits

#def save_results(results, algorithm_name, dataset_name, fuzzy):
def save_results(results, filepath):
	""" 
	Write results to csv.
	Format: url, algorithm, precision, recall, F-score
	"""
	
	# Create the file and write the header line
	if not os.path.isfile(filepath):
		with open(filepath, 'w') as f:
			file_writer = csv.writer(f)
			file_writer.writerow(['Path', 'Algorithm', 'Precision', 'Recall',
								  'F-score', 'Retrieved', 'Hits', 'Relevant'])
	
	# Append a result line
	with open(filepath, 'a') as f:
		file_writer = csv.writer(f)
		file_writer.writerow(results)

def clean_html(html):
	""" Strips the block-attributes from the given HTML document. """
	tree = parse(html, treebuilder='lxml', namespaceHTMLElements=False)
	elems = tree.xpath('//*[@data-block]')
	
	for elem in elems:
		try:
			elem.attrib.pop('data-block')
			elem.attrib.pop('data-block-type')
		except Exception as e:
			print 'Failed to strip block attribute: ', e
			
	return etree.tostring(tree, pretty_print=True)
		
	

def documents(path):
	"""
	A generator function that yields the HTML documents as strings.
	It yields both the original HTML as well as the ground truth HTML.
	"""
	mapping = codecs.open(path, 'rU', 'utf-8')
	
	# Read in the first line
	line = mapping.readline()
	while line:
		# ignore commented lines:
		if line[0] == '#':
			line = mapping.readline()
			continue
		
		# Parse the line to get the url and the path
		split = line[:-2].split(' : ')
		url, path = split[0].strip('"'), split[1].strip('"')
		blockpath = path.split('.html')[0] + '.blocks.html'
		
		# We assume files to be utf-8, since detecting the encoding is really slow
		try:
			f = codecs.open(path, 'rU', 'utf-8')
			ff = codecs.open(blockpath, 'rU', 'utf-8')
			html = f.read()
			blockhtml = ff.read()
		except IOError as e:
			print 'Missing file: ', e
		except Exception as e: # not utf-8
			print 'File is not utf-8: ', e
			# Detect the file encoding
			f = open(path)
			raw_data = f.read()
			encoding = detect(raw_data)['encoding']
			print 'Detected encoding: ', encoding
			try:
				html = raw_data.decode(encoding)
			except Exception as e:
				print 'Decode error: ', e
				html = ''
		finally:
			f.close()
			ff.close()
			
		yield (strip_control_characters(html), strip_control_characters(blockhtml), path)
		line = mapping.readline()

def main(args):
	"""
	Inputs are a dataset and an algorithm to run on that dataset.
	Furthermore you can chose between an exact and a fuzzy match metric
	and whether the input should be the original HTML or the serialized
	DOM tree.
	"""
	
	if len(args) != 5:
		print "Usage:"
		print "%s algorithm_name dataset_path dataset_name fuzzy DOM_html" % sys.argv[0]
		sys.exit(1)
		
	# Read in arguments
	algorithm_name, dataset_path, dataset_name, fuzzy, DOM_html = args
	fuzzy = True if fuzzy == 'True' else False
	DOM_html = True if DOM_html == 'True' else False
	
	# Inputs
#	algorithm_name = 'terrain' # Choices: blockfusion, pagesegmenter, vips, terrain
#	dataset_path = '/opt/dataset2/mapping.txt' # dataset2 is simply a symlink to the active dataset
#	dataset_name = 'popular' # Choices: random, popular
#	fuzzy = True # Choose fuzzy or exact metric
#	DOM_html = True #  Choose original or DOM Html
	
	# We write the input options into the filename of the resulting csv file
	metric = 'fuzzy' if fuzzy else 'exact'
	html_type = 'dom' if DOM_html else 'html'
	filename = algorithm_name + '-' + dataset_name + '-' + metric + '-' + html_type

	# We figure out the run number from the existing files
	try:
		number = int([x.split('.')[0][-1] for x in os.listdir(filedir + '/results/') 
					  if x.startswith(filename)][-1]) + 1
	except:
		number = 1
		
	result_filepath = filedir + '/results/' + filename + '-' + str(number) + '.csv'
	
	# Read in the ground truth
	i = 0
	for original_html, block_html, path in documents(dataset_path + '/mapping.txt'):
		i += 1
		url = fileserver + path.split(dataset_path)[1]
#		print url
		
		# Choose either the original HTML or the serialized DOM HTML
		if DOM_html:
			dom_path = path.split('.html')[0] + '.dom.html'
			url = fileserver + dom_path.split(dataset_path)[1]
			original_html = clean_html(block_html)
			
		# Run terrain algorithm
		if algorithm_name == 'terrain':
			html = run_algorithm(cmd='./algorithms/terrain.py', url=url)

		# Run blockfusion
		elif algorithm_name == 'blockfusion':
			blocktext = boilerpip.algorithm(original_html)
			parsed = parser(blocktext)
			html = mapper(original_html, parsed)

		# Run PageSegmenter
		elif algorithm_name == 'pagesegmenter':
			blocks, tree = semantic_partitioner.algorithm(original_html)
			html = semantic_partitioner.mapper(blocks, tree)
		
		# Run VIPS
		elif algorithm_name == 'vips':
			xml = vips.algorithm(url)
			html = vips.mapper(xml, original_html, i)
		else:
			print 'Unknown algorithm: ', algorithm_name

		# Read in the ground truth
		blocks1 = get_blocks(block_html)
		
		# Get the detected blocks
		blocks2 = get_blocks(html)
		
		# Take the intersection to get exact matches
		scores = evaluate(blocks2, blocks1, path, fuzzy=fuzzy) # prints results
		save_results([path, algorithm_name] + scores, result_filepath)
		
		# Write resulting HTML to disk
#		with open(filedir + '/test-' + algorithm_name + '/file' + str(i), 'w') as f:
#			f.write(html)

#		print html
#		return

if __name__ == "__main__":
	main(sys.argv[1:])
	
	