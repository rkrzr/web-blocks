#!/usr/bin/env python
"""
Run the VIPS page segmentation algorithm on the the given website.

Input: url
Output: The content structure of the page as XML.
"""

from sh import java # @UnresolvedImport
import os
from lxml import etree
from lxml.html import tostring, html5parser	
import difflib
import html5lib
import copy
import sys

def algorithm(url):
	""" We run the algorithm in a subprocess and receive the result on stdout. """
	result = java('-jar', os.path.dirname(__file__) + '/lib/vips.jar', url)
	return str(result)

def mapper(xml, html, count):
	""" Maps the xml back onto the HTML and returns that. """
	xml_parser = etree.XMLParser(recover=True)
	try:
		xml_tree = etree.fromstring(xml, xml_parser)
	except Exception as e:
		print 'XML fail: ', e
		print 'XML: ', xml
		print 'Did you forget to start the dataset server?'
		return ''
	
#	print etree.tostring(xml_tree, pretty_print=True)
#	sys.exit(0)
	 
	texts = [] # the text of each block in the tree
	
	# Parse html into an lxml.etree (lxml's enhanced version of an ElementTree)
	parser = html5lib.HTMLParser(tree=html5lib.getTreeBuilder("lxml"), namespaceHTMLElements=False)
	tree = html5parser.fromstring(html, parser=parser)
	
#	print etree.tostring(tree, pretty_print=True)
#	sys.exit(0)
	
	# Append style to view blocks
	elem = etree.Element('style', type="text/css")
	elem.text = '[data-block] {outline: 1px solid blue !important; box-shadow: inset 0 0 0 10px rgba(220,221,222,1), 0 0 0 1px blue !important;}'
	tree.xpath('//head')[0].append(elem)

	# Go through the xml tree and collect block texts
	for elem in xml_tree.iter():
		if elem.tag == 'LayoutNode':
			text = elem.get('Content')
			if text:
				texts.append(text)
	
	# TESTING
#	content = '\n'.join(texts)	
#	with open(os.getcwd() + '/test-vips/text' + str(count), 'w') as f:
#		f.write(content)
				
	# Walk the html and map the texts back onto it
	new_tree = treewalker(tree, texts)
	result = etree.tostring(new_tree, pretty_print=True)
	
	return result

def treewalker(tree, texts):
	"""
	Walks through the html tree and maps the texts back onto the nodes.
	
	Problem: Walks the tree only once, so if it can't find a text all
	texts after it will also not be added...
	"""
#	# Inefficient brute-force version
#	text = texts.pop(0)
	
#	for text in texts:
#		for element in tree.iter():
#			node_text = tostring(element, encoding='unicode', method='text').strip()
#			
#			if node_text:
#				# Check if the text of the whole node with children matches
#				if difflib.SequenceMatcher(None, text, node_text).ratio() > 0.7:
#					add_block(element)
#					break
#				# Check if just the element text matches
#				elif element.text and difflib.SequenceMatcher(None, text, element.text.strip()).ratio() > 0.7:
#					add_block(element, wrap=True)
#					break

	iterator = list(tree.iter())
	for text in texts:
		element, wrap, nr_elements = walk_tree(text, iterator)
		
		# We add the found element and remember our position in the tree
		if element is not None:
			add_block(element, wrap)
			iterator = iterator[nr_elements:] # we only search through the remainder of the list

	return tree

def walk_tree(text, iterator):
	""" Walks the tree from the given element and sees if the text matches the node text. """
	nr_elements = 0
	text = text.replace(' ', '').strip()

	# Walk the tree in document order
	for element in iterator:
		# skip over the <head> and <title>
		if element.tag == 'head' or element.tag == 'title':
			continue
		nr_elements += 1

		if isinstance(element, etree._Comment): # Skip comments
			continue
		
		node_text = tostring(element, encoding='unicode', method='text').replace('\r', '').replace('\n', '').replace(' ', '').strip()
		
		if node_text:
			element_text = element.text.replace('\r', '').replace('\n', '').replace(' ', '').strip() if element.text else ''
#			print 'counter: ', nr_elements, element_text
			
			# Check if the text of the whole node with children matches
			if difflib.SequenceMatcher(None, text, node_text).ratio() > 0.85:
#				print 'text: ', text
#				print 'node text: ', node_text
#				print 'MATCH: ', text
				return (element, False, nr_elements - 1 + len([1 for _ in element.iter()])) # skip over all descendants as well
#				return (element, False, nr_elements)
			
			# Check if just the element text matches
			elif element_text:
				hit = walk_siblings(element, text, nr_elements)
				if hit:
					return hit
	
	return (None, False, 0) # We failed to find the text

def walk_siblings(element, text, nr_elements):
	""" Add text of subsequent siblings as long as the match ratio gets better. """
	element_text = element.text.replace('\r', '').replace('\n', '').replace(' ', '').strip() if element.text else ''

	if not element_text:
		return None

	prefix = text[:len(element_text)]

	if difflib.SequenceMatcher(None, prefix, element_text).ratio() > 0.85: # startswith
		total_ratio = difflib.SequenceMatcher(None, text, element_text).ratio()
		next_sibling = element.getnext()
		
		if next_sibling is not None:
#			sibling_text = next_sibling.text.replace('\r', '').replace('\n', '').replace(' ', '').strip() if next_sibling.text else ''
			sibling_text = tostring(next_sibling, encoding='unicode', method='text').replace('\r', '').replace('\n', '').replace(' ', '').strip() # Experiment
	
			total_text = element_text + sibling_text
			next_ratio = difflib.SequenceMatcher(None, text, total_text).ratio()
	
			# Exit with only the element text or nothing
			if next_ratio < total_ratio:
				if total_ratio > 0.7:
					return (element, True, nr_elements)
				return None
			
			elements = [element]

			# Get siblings as long as they improve the ratio
			while next_ratio > total_ratio:
				total_ratio = next_ratio
				nr_elements += 1
				elements.append(next_sibling)

				next_sibling = next_sibling.getnext()
				if next_sibling is None: break
#				sibling_text = next_sibling.text.replace('\r', '').replace('\n', '').replace(' ', '').strip() if next_sibling.text else ''
				sibling_text = tostring(next_sibling, encoding='unicode', method='text').replace('\r', '').replace('\n', '').replace(' ', '').strip() # Experiment
				total_text = total_text + sibling_text
				next_ratio = difflib.SequenceMatcher(None, text, total_text).ratio()
				
			# Wrap siblings in new div
			div = wrap_elements(elements) # [element, next_sibling])
			
			# Return if the result is good enough
			if total_ratio > 0.7:
				return (div, False, nr_elements)
			return None
		
		else: # just add the element text up to now (if it is good enough)
			if total_ratio > 0.7:
				return (element, True, nr_elements)
			return None
	

def wrap_elements(elements):
	""" Wraps the given elements in a new div. """
	if not elements:
		return
	
	# Create new div at the position of the first wrapped element
	div = etree.Element('div')
	parent = elements[0].getparent()
	parent.insert(parent.index(elements[0]), div)

	# Reattach elements
	for element in elements:
		div.append(element)
		
	return div


def add_block(element, wrap=False):
	"""
	Modifies the given tree element in-place by adding the block identifier
	and the type of the block.
	"""
	# Create a new element wrapping the text
	if wrap:
		text = element.text
		# Remove the text from the parent
		element.text = ''
		# Create new div wrapping the text
		new_element = etree.Element("div")
		element.insert(0, new_element)
		element = new_element
		element.text = text
		
	# Add block identifier
	try:
		element.set('data-block', '1')
		element.set('data-block-type', 'None')
	except Exception as e:
		print 'Failed to add attribute: ', e
		print 'type: ', type(element)
		print 'dir: ' , dir(element)
		
def main():
	xml = algorithm('http://www.selectorgadget.com')
	result = mapper(xml)
	
	print result
	
if __name__ == '__main__':
	main()
