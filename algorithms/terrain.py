#!/usr/bin/env python

import sys
import gtk
import pywebkitgtk
import nltk

def web_terrain(doc):
	"""
	The web-terrain algorithm takes an HTML document as string and attempts
	to recognize all semantic blocks by analyzing the 3D-geometry of the
	rendered page.
	"""
	return doc
	
class WebTerrain():
	"""
	Runs the WebTerrain algorithm.
	"""
	def __init__(self, webview):
		self.wv = webview
		self.html = ''
		self.document = ''
		
	def run(self):
		self.wv.SetDocumentLoadedCallback(self._doc_loaded)
		
	def highlight_blocks(self, elem, level=None, merge=True):
		"""
		Takes a tree node and returns a list of tree nodes.
		
		Recurses into the given node until it has zero, or more than one, real
		children (visible elements). Then returns those children.
		
		As a side-effect it highlights the real children.
		"""
		# Get the children of body
		real_children = self.get_children(elem, merge)
		
		# A leaf is automatically also a block if we reach it
		if len(real_children) == 0:
			self.highlight(elem, level)
			return [elem]
		
		# Recurse into single children
		elif len(real_children) == 1:
			return self.highlight_blocks(real_children[0], level, merge)
			
		# If we have siblings, we make them blocks
		else:
			# Highlight them
			for child in real_children:
				self.highlight(child, level)
			return real_children
		
	def get_children(self, elem, merge=True):
		"""
		Checks children of an element for their width and height and returns
		them if they are visible.
		"""
		children = elem.children # Full copy?
		real_children = []
		
		# parent area
		area = elem.offsetHeight * elem.offsetWidth * 0.9
		
		iterator = xrange(children.length).__iter__()
		for index in iterator:
#			print 'Nr of children: ', children.length
#			print 'index: ', index
			el = children.item(index)
			
			if not el:
				break
			
			height = el.offsetHeight
			width = el.offsetWidth
#			print 'Height: ' + str(height) + ' Width: ' + str(width)
			
			# Heuristic: Filter out invisible ones
			if height == 0 and width == 0:
				continue
			
			# Heuristic: Filter out separator tags
			elif el.tagName.lower() in ['hr']:
#			elif self.is_separator(el):
				continue
				
			# Heuristic: Filter out blocks smaller than 50x250
#			if height < 40 or width < 200:
#				continue

			# Heuristic: merge headers with subsequent texts
			elif merge and self.is_header(el):
#				print 'A header!'
				div, i = self.heuristic_merge_header(el)
				real_children.append(div)
				
				# skip items
#				for x in xrange(1, i):
#					iterator.next()
				
			# TODO Heuristic: merge textblocks with similar text density
			# TODO Heuristic: look at equal sibling height
			
			# Heuristic: If one child covers more than 90% of the area, recurse into it
			elif area < height * width:
#				print 'area: ', area
#				print 'hw: ', height * width
				
				return [el]
			
			else:					
				real_children.append(el)
		
		return real_children
	
	def heuristic_merge_header(self, el):
		""" Merges sibling elements from the given element to the next separator. """
		# Merge subsequent ones as long as the text density is good?
		# Create new div at the position of the first wrapped element
		index = 0
		div = self.document.createElement('div')
		parent = el.parentNode

		parent.insertBefore(div, el)

		# Move elements to new div
		next = el.nextSibling
#		print 'next: ', next.nodeValue
		div.appendChild(parent.removeChild(el))
		
		# Add all subsequent elements up to the next separator to the block
		while next:
			if self.is_separator(next):
#				print 'totally breaking'
				break
#			print 'textContent: ', next.textContent.splitlines()
			index += 1
			sibling = next.nextSibling
			div.appendChild(parent.removeChild(next))
			next = sibling
			
		return div, index
	
	def is_header(self, el):
		"""
		This heuristic tries to find out whether a given element is a header.
		
		We first check whether we have an h1,h2,h3 or h4 tag. And we also check
		whether the text is rendered with a large font size.
		"""
		# document.defaultView.getComputedStyle(document.documentElement.children[1].children[2], '').getPropertyValue('font-size')
		if hasattr(next, 'tagName') and  el.tagName.lower() in ['h1', 'h2', 'h3', 'h4']:
			return True
		
		# get font_size
		if isinstance(el, pywebkitgtk.Element):
			font_size = self.document.defaultView.getComputedStyle(el, '').getPropertyValue('font-size')
			if font_size:
				font_size = int(font_size[:-2])
				
				if font_size > 18: # TODO
					return True
				
		return False
	
	def is_separator(self, el):
		"""
		This heuristic tries to find out whether a given element is a separator.
		
		It looks for separator tags, such as hr, multiple br's, table?.
		Headers are considered separators as well.
		Also images that have separator dimensions (i.e. lines).
		And for menus and linked lists.
		
		What about css borders?
		"""
		is_hr = lambda x: hasattr(x, 'tagName') and  x.tagName.lower() == 'hr'
		
		return any([is_hr(el), WebTerrain.multiple_brs(el), self.is_header(el), WebTerrain.separator_img(el)])
#		return any([is_hr(el), WebTerrain.multiple_brs(el), WebTerrain.separator_img(el)])
		
	
	@staticmethod
	def separator_img(el):
		""" Heuristic that checks whether we have an image that looks like a line. """
		return (hasattr(el, 'tagName')
				and el.tagName.lower() == 'img'
				and el.height / float(el.width) < 0.1)
		
	@staticmethod
	def multiple_brs(el):
		""" Test if we have at least two <br>'s in a row. """
		return (hasattr(el, 'tagName')
				and el.tagName.lower() == 'br'
				and el.nextSibling
				and hasattr(el.nextSibling, 'tagName')
				and el.nextSibling.tagName.lower() == 'br')
		
	@staticmethod
	def text_density_heuristic(el1, el2):
		"""
		Compares the text densities of two adjacent elements.
		
		Returns True if the densities are similar and False otherwise.
		"""
		return abs(block_density(el1) - block_density(el2)) < 0.1 # TODO: Pick a reasonable number
		
	@staticmethod
	def block_density(el):
		"""
		Measures the density D of an element.
		
		D(el) = (Number of tokens in el)/(Number of lines in el)
		
		where the line length is fixed to 80 characters.
		"""
		text = el.textContent          # textContent already wraps at 80 chars
		lines = len(text.splitlines())
		tokens = len(nltk.word_tokenize(text))
		
		return tokens/float(lines)
		
	def highlight(self, elem, level):
		""" Highlight each block by drawing a red border around it. """
		elem.style.setProperty('border','1px solid #E8272C', '100')
#		elem.style.setProperty('boxShadow','inset 0 0 0 10px rgba(220,221,222,1), 0 0 0 1px red', '100')
#		elem.style.setProperty('backgroundColor','yellow', '100')
		elem.setAttribute('data-block', level)
				
	def _doc_loaded(self, *args):
		""" This callback is triggered by the onload-event. """
		# Get the DOM
		self.document = self.wv.GetDomDocument()

		# Get the body element
		elem = self.document.body
		
		# Get height and width of the chosen element
		height = elem.offsetHeight
		width = elem.offsetWidth
#		print 'Height: ' + str(height) + ' Width: ' + str(width)
		
		# Highlight blocks
		blocks = self.highlight_blocks(self.document.body, level='1')
		
		# Highlight sub-blocks
		for block in blocks:
			self.highlight_blocks(block, level='2', merge=False)
			
		# dump html
		self.html = self.document.body.parentNode.outerHTML
		print self.html
				
#		# write it to file
#		with open('/home/rob/coding/python/thesis/web-blocks/test/out.html', 'w') as f:
#			f.write(sefl.html)

		# close the gtk window
		gtk.main_quit()
		
def main():
	if len(sys.argv) != 2:
		print "Please provide one argument: ./terrain.py url"
		return None

	url = sys.argv[1]
	
#	web_terrain(doc)
	# url = "http://arstechnica.com/information-technology/2009/07/how-to-build-a-desktop-wysiwyg-editor-with-webkit-and-html-5/"
#	url = "http://www.selectorgadget.com/"
#	url = "http://sucha.eu/~okla/"
#	url = 'http://localhost:8080/lxml.de/lxml.de/xpathxslt.html'
	
	wv = pywebkitgtk.WebView(1024, 768, url=url)
	
	b = WebTerrain(wv)
	b.run()
	
	gtk.main()

if __name__ == '__main__':
	main()
	