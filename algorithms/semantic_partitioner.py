#!/usr/bin/env python
"""
Run the 'semantic partitioner' page segmentation algorithm on the the given website.

Input: The HTML source of the page
Output: The text blocks the website consists of
"""
from lxml.html import tostring, html5parser, fromstring
from lxml import etree
import html5lib

import sys
import codecs
import locale
from collections import deque
import simplejson
import math
from tidylib import tidy_document
from itertools import combinations, chain

# Make python and stdout understand utf-8
#sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)

def algorithm(page, init=False):
	"""
	Parses the given text page and calls the actual algorithm.
	
	Returns a list of lists of DOM nodes (mapping to blocks) and the parsed DOM tree.
	"""
	
	# Parse html into the DOM-like 'minidom'
	parser = html5lib.HTMLParser(tree=html5lib.getTreeBuilder("dom"), namespaceHTMLElements=False)
#	tree = html5parser.fromstring(page, parser=parser)
	tree = parser.parse(page) # minidom tree!
	
	# get counters of all leaf nodes
	counters = get_path_counters(tree)
	
	# add entropy to each node
	add_entropies(tree, counters)
	
	# get the median path entropy
	median = get_median(tree, counters)
	
	# Run algorithm
	segments = page_segmenter([tree], median)
	
	# Recurse into sub-blocks
	sub_segments = []
	for segment in segments:
		sub_segments.extend(page_segmenter(segment, median))
	segments.extend(sub_segments)

	# Style
	text = '[data-block] {outline: 1px solid blue !important; box-shadow: inset 0 0 0 10px rgba(220,221,222,1), 0 0 0 1px blue !important;}'
	style = tree.createElement('style')
	style.attributes['type'] = 'text/css'
	style.appendChild(tree.createTextNode(text))
	head = tree.getElementsByTagName('head')[0]
	head.appendChild(style)
	
	return (segments, tree)

def add_entropies(tree, counters):
	""" Go through the tree once and attach the entropy to each node. """
	for node in breadth_first(tree):
		setattr(node, 'entropy', entropy(node, counters))

def get_path(node, elems):
	""" Get the path of a node in a DOM tree. """
	if node.parentNode:
		return get_path(node.parentNode, [node.nodeName] + elems)
	else:
		return '/' + '/'.join([node.nodeName] + elems)
	
def get_leaves(node):
	""" Get all leaves of a node in a DOM tree. """
	if node.childNodes:
		return reduce(lambda x,y: x + y, [get_leaves(x) for x in node.childNodes])
	else:
		return [node]
	
def breadth_first(node):
	""" Go breadth-first through the DOM tree. """
	children = [node] + [x for x in node.childNodes]
	
	while children:
		child = children.pop(0) # Fucks up the tree !!!
		yield child
		
		# add next level to stack
		if child.childNodes:
			children += [x for x in child.childNodes]

def get_median(root, counters):
	""" Calculate entropy for every node in the tree and take the median. """
	entropies = []
	
#	for node in root.iter():
	for node in breadth_first(root):
#		entropies.append(entropy(node, counters))
		entropies.append(node.entropy)
	
	# Filter out zeros
	entropies = [x for x in entropies if x]
	
	# Order and get median
	entropies.sort()
	length = len(entropies)
	
#	print 'entropies: ', entropies
	
	if not length % 2:
		return (entropies[length/2] + entropies[length/2 - 1]) / 2.0
	else:
		return entropies[length/2]
	
def ranges(elements):
	"""
	A generator for all possible ranges of a set (excluding the set itself).
	
	Starts with the longest ranges, so e.g. for the
	set [1,2,3] it would generate:
	[[1,2], [2,3], [1], [2], [3]].
	"""
	length = len(elements)
	if length > 0:
		for i in (range(length, 0, -1)):
			for x in range(length - i + 1):
				if length == 1:
					yield elements[x:x+i]
				elif len(elements[x:x+i]) < length:
					yield elements[x:x+i]
	else:
		yield []
	
def page_segmenter(nodes, median):
	""" 
	Our implementation of the algorithm, as described in the paper
	'Semantic Partitioning of Web pages'.
	
	Input: [root_node]
	Output: A set of segments.
	"""
	if not nodes: return []
	
	r = ranges(nodes)
	result = []
	for subset in r:
		print len(subset)
		average = sum([node.entropy for node in subset]) / float(len(subset))
		
		# Check if we have a segment
		# TODO: Single median?
		if average <= median * 2:  # double median seems to give much better results than median alone
			result.append(subset)
			result.extend(split_recursion(subset, nodes, median))
			return result
			
		# Recurse into children of single nodes
		elif len(subset) == 1:
			result.extend(page_segmenter(subset[0].childNodes, median))
			result.extend(split_recursion(subset, nodes, median)) # Recurse into siblings
			return result
		
def split_recursion(subset, nodes, median):
	""" We recurse into the left and right siblings of a subset. """
	# Get the positions of the first and last element of the subset in nodes
	first = (i for i, x in enumerate(nodes) if x == subset[0]).next()
	last = (i for i, x in enumerate(nodes) if x == subset[-1]).next()
	result = []
	
	# Get the elements before and after the subset in nodes
	before = nodes[:first]
	after = nodes[last + 1:]
	
	# Recurse into them
	result.extend(page_segmenter(before, median))
	result.extend(page_segmenter(after, median))
	
	return result
	
def entropy(node, counters):
	"""
	Calculate the entropy of the given node by first getting all the leaf
	nodes of that node and then getting the probabilities of their paths.
	"""
	leaves = get_leaves(node)
	sum = 0
	
	for leaf in leaves:
		path = get_path(leaf, [])
		prob = counters[path]['weight']
		summand = prob * math.log10(prob)
		sum += summand
		
	sum = sum * (-1)
	
	return sum

def get_path_counters(root):
	"""
	Calculate the path entropy for a node by looking at the root-to-leaf path
	of all its leaf descendants.
	"""
	result = {}
	leaves = get_leaves(root)
	total = len(leaves)
	
	for leaf in leaves:
		# Get root-to-leaf path
		path = get_path(leaf, [])
		
		if path in result:
			result[path]['count'] += 1
			result[path]['weight'] = float(result[path]['count'])/total
		else:
			result[path] = {'weight' : 1.0/total, 'count' : 1}
	
	return result

def is_visible(node):
	""" Checks whether a node is visible. """
	# TEXT_NODE = 3
	if node.nodeType == 3 and not node.nodeValue.replace(' ', '').replace('\n', ''):
		return False
	elif hasattr(node, 'tagName') and node.tagName.lower() in ['script', 'title', 'style', 'link']:
		return False
	else:
		return True

def mapper(segments, tree):
	"""
	Map the segments back onto the HTML.
	Segments are contiguous! sets of nodes in the tree
	"""
	for segment in segments:
#		print 'segment: ', segment
		
#		# special case to not add empty text nodes
#		if len(segment) == 1 and segment[0].nodeType == 3 and not segment[0].nodeValue.replace(' ', '').replace('\n', ''):
#			continue
		
		# If none of the nodes in the segment is visible we skip it
		if not any([is_visible(x) for x in segment]):
			continue
		
		# insert new element wrapping segment
		parent = segment[0].parentNode
		
		# Skip doctype elements
		if not parent:
			continue
		
		# special case for tables, since you cannot put divs inside it
		if hasattr(parent, 'tagName') and parent.tagName in ['tbody', 'table']:
			parent.setAttribute('data-block', '1')
			continue # no need to move anything
		else:
			div = tree.createElement('div')
			div.setAttribute('data-block', '1')
			parent.insertBefore(div, segment[0])
		
		# Move all nodes in the segment to the new container div
		for node in segment:
#			print 'node: ', node.parentNode, node.nodeType, node.toxml()
			try:
				parent.removeChild(node)
				
#				if node.nodeType == 3:
#					div.appendChild(str(node) + 'bla')
#				else:
				div.appendChild(node)
			except Exception as e:
#				print 'Cannot move node: ', e
				pass

#		for element in segment:
##			try:
##				element.setAttribute('data-block', '1')
##			except AttributeError:
#			# check if we have a non-empty text node
#			if element.nodeType == 3 and element.nodeValue.replace(' ', '').replace('\n', ''):
#				# Wrap the text in an additional div and make it a block
#				parent = element.parentNode
#				div = tree.createElement('div')
#				div.setAttribute('data-block', '1')
#				parent.insertBefore(div, element)
#				parent.removeChild(element)
#				div.appendChild(element)
	
	# Serialize the tree
	try:
		xml = tree.toxml()
	except Exception as e:
		
		# Remove comments from the tree
		for node in breadth_first(tree):
			if node.nodeType == 8: # comment nodes are type 8...
				parentNode = node.parentNode
				if parentNode:
					parentNode.removeChild(node)				
		xml = tree.toxml()

	# We use HTML Tidy to turn the XML into HTML
	document, errors = tidy_document(xml, options={'numeric-entities':1})
	return document
	
def main():
	url = '/opt/dataset2/www.selectorgadget.com/www.selectorgadget.com/index.html'
	with codecs.open(url, 'rU', 'utf-8') as f:
		html = f.read()
	segments, tree = algorithm(html)
	result = mapper(segments, tree)
	
	print result

if __name__ == '__main__':
	main()