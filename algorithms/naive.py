#!/usr/bin/env python

import gtk
import pywebkitgtk

class NaiveAlgorithm():
	def __init__(self, webview):
		self.wv = webview
		
	def run(self):
		self.wv.SetDocumentLoadedCallback(self._doc_loaded)
		
	def highlight_blocks(self, elem):
		"""
		Takes a tree node and returns a list of tree nodes.
		
		Recurses into the given node until it has zero or more
		than one real child (is visible). Then returns those children.
		
		As a side-effect it highlights the real children.
		"""
		# Get the children of body
		real_children = self.get_children(elem)
		
		if len(real_children) == 0:
			self.highlight(elem)
			return [elem]
		
		elif len(real_children) == 1:
			return self.highlight_blocks(real_children[0])
			
		else:
			# Highlight them
			for child in real_children:
				self.highlight(child)
			return real_children
		
	def get_children(self, elem):
		"""
		Checks children of an element for their width and height and returns
		them if they are visible.
		"""
		children = elem.children
		
		real_children = []
		
		for index in range(0, children.length):
			elem = children.item(index)
			print elem
			height = elem.offsetHeight
			width = elem.offsetWidth
			print 'Height: ' + str(height) + ' Width: ' + str(width)
			
			if height > 0 and width > 0:
				real_children.append(elem)
		
		return real_children
	
	def highlight(self, elem):
		elem.style.setProperty('border','1px solid #E8272C', '100')
				
	def _doc_loaded(self, *args):
		""" This callback is triggered by the onload-event. """
		# Get the DOM
		doc = self.wv.GetDomDocument()

		# Get the body element
		elem = doc.body
		
		# Get height and width of the chosen element
		height = elem.offsetHeight
		width = elem.offsetWidth
		print 'Height: ' + str(height) + ' Width: ' + str(width)
		
		# Highlight blocks
		blocks = self.highlight_blocks(doc.body)
		
		# Highlight sub-blocks
		for block in blocks:
			self.highlight_blocks(block)
			

def main():
	url = "http://arstechnica.com/information-technology/2009/07/how-to-build-a-desktop-wysiwyg-editor-with-webkit-and-html-5/"
	# url = "http://www.selectorgadget.com/"
	# url = "http://sucha.eu/~okla/"
	wv = pywebkitgtk.WebView(1024, 768, url=url)
	b = NaiveAlgorithm(wv)
	b.run()
	
	print 'start'
	
	gtk.main()

if __name__ == '__main__':
	main()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	