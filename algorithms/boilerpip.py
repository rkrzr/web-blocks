#!/usr/bin/env python
"""
Run the boilerpipe page segmentation algorithm on the the given website.

Input: url
Output: The text blocks the website consists of
"""

from boilerpipe.extract import Extractor
import sys
import codecs
import locale


# Make python and stdout understand utf-8
sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)

def algorithm(page):
	""" Run the wrapped algorithm via jpype. """
	result = Extractor(html=page, extractor='BlockExtractor')
	blocks = result.getTextBlocks().toString()
	
	return blocks 

def main():	
#	sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
	url = 'http://www.selectorgadget.com/'
#	url = 'http://www.site2mobile.com/'
	result = Extractor(extractor='BlockExtractor', url=url)
	blocks = result.getTextBlocks().toString()

if __name__ == '__main__':
	main()