#!/usr/bin/env python

import sh

platform = sh.Command("/home/rob/coding/python/thesis/web-blocks/block_platform.py")

def main():
	"""
	We can't run both datasets at the same time, because we need to serve those
	files via HTTP locally and so far the port is hardcoded to 8080, and only one
	server can listen on that port then.
	"""
	algorithm = 'pagesegmenter'
	datasets = {'random' : '/opt/dataset-random'}
#	datasets = {'popular' : '/opt/dataset-popular'}
	fuzzys = ['True', 'False']
	DOM_htmls = ['True', 'False']
	
	for name, path in datasets.iteritems():
		for fuzzy in fuzzys:
			for DOM_html in DOM_htmls:
				print 'Running: ', algorithm, path, name, fuzzy, DOM_html
				print platform(algorithm, path, name, fuzzy, DOM_html)

if __name__ == "__main__":
	main()
	